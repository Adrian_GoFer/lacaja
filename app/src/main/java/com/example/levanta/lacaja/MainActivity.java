package com.example.levanta.lacaja;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.TextView;

import com.example.levanta.lacaja.Films.FilmGenerator;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final FilmGenerator filmGenerator = new FilmGenerator();
        final Button button = findViewById(R.id.goodluckbtn);
        final WebView webView = findViewById(R.id.webview_info);
        final TextView textView = findViewById(R.id.textView_id);

        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                webView.loadUrl("about:blank");
                final int id = filmGenerator.RandomIndex();
                textView.setText(String.valueOf(id));
                webView.loadUrl("https://filmaffinity-api.mybluemix.net/api/film/byId?id=%2Fes%2Ffilm"+ id +".html");
            }
        });
    }
}
