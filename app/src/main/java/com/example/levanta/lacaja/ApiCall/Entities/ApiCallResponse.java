package com.example.levanta.lacaja.ApiCall.Entities;

/**
 * Created by Levanta on 07/03/2018.
 */

public class ApiCallResponse {

    private String Calificación;
    private String Título;
    private String Año;
    private String Duración;
    private String País;
    private String Director;
    private String Guion;
    private String Musica;
    private String Fotografía;
    private String Reparto;
    private String Productora;
    private String Género;
    private String Sinopsis;

    public String getCalificación() {
        return Calificación;
    }

    public void setCalificación(String calificación) {
        Calificación = calificación;
    }

    public String getTítulo() {
        return Título;
    }

    public void setTítulo(String título) {
        Título = título;
    }

    public String getAño() {
        return Año;
    }

    public void setAño(String año) {
        Año = año;
    }

    public String getDuración() {
        return Duración;
    }

    public void setDuración(String duración) {
        Duración = duración;
    }

    public String getPaís() {
        return País;
    }

    public void setPaís(String país) {
        País = país;
    }

    public String getDirector() {
        return Director;
    }

    public void setDirector(String director) {
        Director = director;
    }

    public String getGuion() {
        return Guion;
    }

    public void setGuion(String guion) {
        Guion = guion;
    }

    public String getMusica() {
        return Musica;
    }

    public void setMusica(String musica) {
        Musica = musica;
    }

    public String getFotografía() {
        return Fotografía;
    }

    public void setFotografía(String fotografía) {
        Fotografía = fotografía;
    }

    public String getReparto() {
        return Reparto;
    }

    public void setReparto(String reparto) {
        Reparto = reparto;
    }

    public String getProductora() {
        return Productora;
    }

    public void setProductora(String productora) {
        Productora = productora;
    }

    public String getGénero() {
        return Género;
    }

    public void setGénero(String género) {
        Género = género;
    }

    public String getSinopsis() {
        return Sinopsis;
    }

    public void setSinopsis(String sinopsis) {
        Sinopsis = sinopsis;
    }
}
