package com.example.levanta.lacaja.ApiCall;

import com.example.levanta.lacaja.ApiCall.Entities.ApiCallRequest;
import com.example.levanta.lacaja.ApiCall.Entities.ApiCallResponse;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;

/**
 * Created by Levanta on 07/03/2018.
 */

public class ApiCall implements IApiCall {
    @Override
    public ApiCallResponse GetFilm(ApiCallRequest request) throws NoSuchAlgorithmException {
        try {
            String url = "https://filmaffinity-api.mybluemix.net/api/film/byId?id=%2Fes%2Ffilm" + request.GetId() + ".html";
            URL completeUrl = new URL(url);
            SSLContext sslContext = SSLContext.getInstance("TLSv1.2");
            sslContext.init(null, null, new java.security.SecureRandom());
            HttpsURLConnection conn = (HttpsURLConnection) completeUrl.openConnection();
            conn.setSSLSocketFactory(sslContext.getSocketFactory());
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Accept", "application/json");
            conn.setRequestProperty("If-None-Match:", "W/\"3e3-sobO641idI6sb4Leg4Q7oQ\"");

            if (conn.getResponseCode() != 200) {
                throw new RuntimeException("Failed : HTTP error code : "
                        + conn.getResponseCode());
            }
            BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));
            String output;
            System.out.println("Output from Server .... \n");
            while ((output = br.readLine()) != null) {
                System.out.println(output);
            }
            conn.disconnect();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (KeyManagementException e) {
            e.printStackTrace();
        }
        return null;
    }
}


