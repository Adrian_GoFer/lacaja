package com.example.levanta.lacaja.ApiCall;

import com.example.levanta.lacaja.ApiCall.Entities.ApiCallRequest;
import com.example.levanta.lacaja.ApiCall.Entities.ApiCallResponse;

import java.security.NoSuchAlgorithmException;

public interface IApiCall{
    ApiCallResponse GetFilm(ApiCallRequest request) throws NoSuchAlgorithmException;
}
