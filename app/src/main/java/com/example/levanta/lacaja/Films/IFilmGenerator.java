package com.example.levanta.lacaja.Films;

import com.example.levanta.lacaja.Films.Entities.Film;

public interface IFilmGenerator{
    Film GetFilm();
    int RandomIndex();
}
