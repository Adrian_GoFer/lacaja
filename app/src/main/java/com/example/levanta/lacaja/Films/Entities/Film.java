package com.example.levanta.lacaja.Films.Entities;

/**
 * Created by Levanta on 07/03/2018.
 */

public class Film {
    private String FilmName;
    private String FilmDuration;
    private String FilmCountry;
    private String FilmPoints;
    private String FilmId;
    private String FilmYear;

    public String getFilmName() {
        return FilmName;
    }

    public void setFilmName(String filmName) {
        FilmName = filmName;
    }

    public String getFilmDuration() {
        return FilmDuration;
    }

    public void setFilmDuration(String filmDuration) {
        FilmDuration = filmDuration;
    }

    public String getFilmCountry() {
        return FilmCountry;
    }

    public void setFilmCountry(String filmCountry) {
        FilmCountry = filmCountry;
    }

    public String getFilmPoints() {
        return FilmPoints;
    }

    public void setFilmPoints(String filmPoints) {
        FilmPoints = filmPoints;
    }

    public String getFilmId() {
        return FilmId;
    }

    public void setFilmId(String filmId) {
        FilmId = filmId;
    }

    public String getFilmYear() {
        return FilmYear;
    }

    public void setFilmYear(String filmYear) {
        FilmYear = filmYear;
    }
}
